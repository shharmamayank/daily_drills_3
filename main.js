const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];




//1. Get all items that are available 

// let present = items.filter(index => {
//     return index.available == true
// })
// console.log(present)

//2. Get all items containing only Vitamin C.

// let VitaminC = items.filter(index => {
//     return index.contains.includes("Vitamin C")
// })
// console.log(VitaminC)

//3. Get all items containing Vitamin A.

// let vitaminA = items.filter(index => {
//     return index.contains == "Vitamin A"
// })
// console.log(vitaminA)

// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
//         and so on for all items and all Vitamins.

let GroupItems = items.reduce((index1, index2) => {
    index2.contains.split(",").map(datrow => {
        if (index1[datrow] == true) {
            index1[datrow] = index1[datrow] + " " + index2.name
        }
        else {
            index1[datrow] = index2.name
        }
        return index2
    })
    return index1
})
console.log([GroupItems])

//5. Sort items based on number of Vitamins they contain.

// let numbersOfVitamins = items.sort((vitamin1, vitamin2) => {
//     let countValue1 = vitamin1.contains.substring(",").length
//     let countValue2 = vitamin2.contains.substring(",").length
//     if (countValue1 > countValue2) {
//         return +1
//     }
//     else {
//         return -1
//     }
// })
// console.log(numbersOfVitamins)